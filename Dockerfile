FROM openjdk:12-alpine

EXPOSE 9999

ADD ./target/*.jar app.jar
 
ENTRYPOINT ["java","-jar","/app.jar"]
