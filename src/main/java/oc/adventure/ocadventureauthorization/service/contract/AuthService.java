package oc.adventure.ocadventureauthorization.service.contract;

import oc.adventure.shared.components.dto.auth.AuthBodyOutDto;

public interface AuthService {
    AuthBodyOutDto getUserLogInfo(String email);
}
