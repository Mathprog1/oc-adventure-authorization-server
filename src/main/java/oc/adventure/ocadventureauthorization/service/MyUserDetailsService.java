package oc.adventure.ocadventureauthorization.service;

import oc.adventure.ocadventureauthorization.service.contract.AuthService;
import oc.adventure.shared.components.dto.auth.AuthBodyOutDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service("userDetailsService")
@Transactional
public class MyUserDetailsService implements UserDetailsService {

    @Autowired
    private AuthService authService;


    @Override
    public UserDetails loadUserByUsername(String userMail) throws UsernameNotFoundException {
        AuthBodyOutDto userLogInfo = authService.getUserLogInfo(userMail);

        if (userLogInfo == null) {
            return new User(
                    " ", " ", true, true, true, true,
                    getGrantedAuthorities(Collections.singletonList("ROLE_USER")));
        }

        return new User(userLogInfo.getMail(), userLogInfo.getPassword(), true, true, true,
                true, getGrantedAuthorities(userLogInfo.getRoleList()));
    }



    private List<GrantedAuthority> getGrantedAuthorities(List<String> privileges) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        for (String privilege : privileges) {
            authorities.add(new SimpleGrantedAuthority(privilege));
        }
        return authorities;
    }
}
