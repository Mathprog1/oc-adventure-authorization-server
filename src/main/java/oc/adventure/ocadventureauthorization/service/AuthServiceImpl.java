package oc.adventure.ocadventureauthorization.service;


import oc.adventure.ocadventureauthorization.service.contract.AuthService;
import oc.adventure.shared.components.dto.auth.AuthBodyOutDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

@Service
public class AuthServiceImpl implements AuthService {

    private final RestTemplate restTemplate;

    private final String USERMS_URL = "http://192.168.99.100:8080/user/";

    @Autowired
    public AuthServiceImpl(RestTemplateBuilder restTemplateBuilder){
        this.restTemplate = restTemplateBuilder.build();
    }


    @Override
    public AuthBodyOutDto getUserLogInfo(String email){
        String url = USERMS_URL + "api/v1/ms/user/login/" + email;
        System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ LOGIN SERVICE WITH : " + url);

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Content-Type", "application/json");
        ResponseEntity<AuthBodyOutDto> entity = restTemplate.exchange(
                url, HttpMethod.GET, new HttpEntity<>(headers),
                AuthBodyOutDto.class);

        return  entity.getBody();
    }

}
