package oc.adventure.ocadventureauthorization.controller;


import oc.adventure.ocadventureauthorization.config.JwtTokenProvider;
import oc.adventure.ocadventureauthorization.service.contract.AuthService;
import oc.adventure.shared.components.dto.auth.AuthBodyInDto;
import oc.adventure.shared.components.dto.auth.AuthBodyOutDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping("/api/v1/ms/auth")
public class AuthController {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    JwtTokenProvider jwtTokenProvider;

    @Autowired
    private AuthService authService;

    @PostMapping("/login/")
    public ResponseEntity login(@RequestBody AuthBodyInDto data) {
        try {
            String username = data.getMail();
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, data.getPassword()));
            AuthBodyOutDto authBodyOutDto = this.authService.getUserLogInfo(username);
            String token = jwtTokenProvider.createToken(username, authBodyOutDto.getRoleList(), authBodyOutDto.getExternalId());
            Map<Object, Object> model = new HashMap<>();
            model.put("username", username);
            model.put("token", token);
            return ok(model);
        } catch (AuthenticationException e) {
            throw new BadCredentialsException("Invalid email/password supplied");
        }
    }

}
